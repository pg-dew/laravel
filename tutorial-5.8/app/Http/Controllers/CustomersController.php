<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Company;

class CustomersController extends Controller
{
    public function list() {
        // $customers = [
        //     "Jone Doe",
        //     "Bob The builder",
        //     "Jane Noon Bow",
        //     "Yeee"
        // ];

        // $customers = Customer::all();
        // dd($customers);

        // $activeCustomers = Customer::where('active', 1)->get();
        $activeCustomers = Customer::active()->get();
        // $inactiveCustomers = Customer::where('active', 0)->get();
        $inactiveCustomers = Customer::inactive()->get();
        // dd($inactiveCustomers);

    
        $companies = Company::all();

        // return view('internal.customers', [
        //     'activeCustomers' => $activeCustomers,
        //     'inactiveCustomers' => $inactiveCustomers
        // ]);

        return view('internal.customers', compact('activeCustomers', 'inactiveCustomers', 'companies'));
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required|min:3',
            'email' => 'required|email',
            'active' => 'required',
            'company_id' => 'required'
            // 'random' => ''   <= Optional
        ]);
        
        // dd($data);


        // $customer = new Customer();
        // $customer->name = request('name');
        // $customer->email = request('email');
        // $customer->active = request('active');
        // $customer->save();
        
        // dd(request('name'));
        
        Customer::create($data);

        return back();
    }
}
